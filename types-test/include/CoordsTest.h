#pragma once

#include <gtest/gtest.h>
#include <Coords.h>


namespace {

using namespace types;


class CoordsTest : public ::testing::Test {
};




TEST_F( CoordsTest, CartesianAndSpherical ) {

    const Cartesian3  ca( 10, 30, 50 );
    const Spherical3  sa = ca;
    const Cartesian3  cca = sa;
    EXPECT_FLOAT_EQ( ca.x(), cca.x() );
    EXPECT_FLOAT_EQ( ca.y(), cca.y() );
    EXPECT_FLOAT_EQ( ca.z(), cca.z() );
}




TEST_F( CoordsTest, CartesianAndVec ) {

    const Cartesian3  ca( 10, 30, 50 );
    const Vec3  va = ca;
    const Cartesian3  cca = va;
    EXPECT_FLOAT_EQ( ca.x(), cca.x() );
    EXPECT_FLOAT_EQ( ca.y(), cca.y() );
    EXPECT_FLOAT_EQ( ca.z(), cca.z() );
}




TEST_F( CoordsTest, SphericalAndVec ) {

    const Spherical3  sa( 10, 30, 50 );
    const Vec3  va = sa;
    const Spherical3  csa = va;
    EXPECT_FLOAT_EQ( sa.rho(), csa.rho() );
    EXPECT_FLOAT_EQ( sa.phi(), csa.phi() );
    EXPECT_FLOAT_EQ( sa.theta(), csa.theta() );
}


} // namespace
