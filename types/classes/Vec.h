#pragma once

#include "../macros.h"
#include "../debug.h"
#include <Eigen/Dense>


namespace types {


template< typename T, size_t D >
class Cartesian;

typedef Cartesian< float, 2 >  Cartesian2;
typedef Cartesian< float, 3 >  Cartesian3;


template< typename T, size_t D >
class Spherical;

typedef Spherical< float, 2 >  Spherical2;
typedef Spherical< float, 3 >  Spherical3;




template< typename T, size_t D >
class Vec : public Eigen::Matrix< T, D, 1 > {
public:
    Vec( T x, T y ) : Eigen::Matrix< T, D, 1 >( x, y ) { static_assert(D == 2, ""); }
    Vec( T x, T y, T z ) : Eigen::Matrix< T, D, 1 >( x, y, z ) { static_assert(D == 3, ""); }

    operator Cartesian2 () const;
    operator Cartesian3 () const;

    operator Spherical2 () const;
    operator Spherical3 () const;
};


typedef Vec< float, 2 >  Vec2;
typedef Vec< float, 3 >  Vec3;


} // types




namespace std {

std::string to_string( const ::types::Vec2& v ) {
    return "[" + std::to_string( v.x() ) + ", " + std::to_string( v.y() ) + "]";
}

std::string to_string( const ::types::Vec3& v ) {
    return "[" + std::to_string( v.x() ) + ", " + std::to_string( v.y() ) + ", " + std::to_string( v.z() ) + "]";
}

} // std
