#pragma once

#include "Vec.h"


namespace types {


template< typename T, size_t D >
class Cartesian {
public:
    typedef Eigen::Matrix< T, D, 1 >  raw_t;


public:
    Cartesian( T x, T y ) : mRaw( x, y ) { static_assert(D == 2, ""); }
    Cartesian( T x, T y, T z ) : mRaw( x, y, z ) { static_assert(D == 3, ""); }

    T x() const { return mRaw[ 0 ]; }
    T y() const { return mRaw[ 1 ]; }
    T z() const { return mRaw[ 2 ]; }

    operator Spherical2 () const;
    operator Spherical3 () const;

    operator Vec2() const { return Vec2( x(), y() ); }
    operator Vec3() const { return Vec3( x(), y(), z() ); }


private:
    raw_t  mRaw;
};


typedef Cartesian< float, 2 >  Cartesian2;
typedef Cartesian< float, 3 >  Cartesian3;




template< typename T, size_t D >
class Spherical {
public:
    typedef Eigen::Matrix< T, D, 1 >  raw_t;


public:
    Spherical( T rho, T phi ) : mRaw( rho, phi ) { static_assert(D == 2, ""); }
    Spherical( T rho, T phi, T theta ) : mRaw( rho, phi, theta ) { static_assert(D == 3, ""); }

    T rho() const   { return mRaw[ 0 ]; }
    T phi() const   { return mRaw[ 1 ]; }
    T theta() const { return mRaw[ 2 ]; }

    operator Cartesian2 () const;
    operator Cartesian3 () const;

    operator Vec2() const { return Vec2( rho(), phi() ); }
    operator Vec3() const { return Vec3( rho(), phi(), theta() ); }


private:
    raw_t  mRaw;
};




template< typename T, size_t D >
Spherical< T, D >::operator Cartesian2 () const {
    static_assert(D == 2, "");
    return Cartesian2(
        rho() * std::cos( phi() ),
        rho() * std::sin( phi() ) );
}


template< typename T, size_t D >
Spherical< T, D >::operator Cartesian3 () const {
    static_assert(D == 3, "");
    return Cartesian3(
        rho() * std::cos( phi() ) * std::sin( theta() ),
        rho() * std::sin( phi() ) * std::sin( theta() ),
        rho() * std::cos( theta() ) );
}




template< typename T, size_t D >
Cartesian< T, D >::operator Spherical2 () const {
    static_assert(D == 2, "");
    const auto  r = std::sqrt( x() * x() + y() * y() );
    return Spherical2(
        r,
        std::atan( y() / x() ) );
}


template< typename T, size_t D >
Cartesian< T, D >::operator Spherical3 () const {
    static_assert(D == 3, "");
    const auto  r = std::sqrt( x() * x() + y() * y() + z() * z() );
    return Spherical3(
        r,
        std::atan( y() / x() ),
        std::acos( z() / r ) );
}




template< typename T, size_t D >
Vec< T, D >::operator Cartesian2 () const {
    return Cartesian2( x(), y() );
}


template< typename T, size_t D >
Vec< T, D >::operator Cartesian3 () const {
    return Cartesian3( x(), y(), z() );
}




template< typename T, size_t D >
Vec< T, D >::operator Spherical2 () const {
    return Spherical2( x(), y() );
}


template< typename T, size_t D >
Vec< T, D >::operator Spherical3 () const {
    return Spherical3( x(), y(), z() );
}



} // types




namespace std {

// @todo std::string& toString( const ::types::X& );

} // std
